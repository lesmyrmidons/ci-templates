# [0.3.0](https://gitlab.arcons.io/arcons/ci-templates/compare/v0.2.3...0.3.0) (2022-06-12)


### Features

* **gitlab-ci:** Add validator for template gitlab-ci ([7611df0](https://gitlab.arcons.io/arcons/ci-templates/commit/7611df0eed7fddb53c243e0240a74784cc9a8ae7))
